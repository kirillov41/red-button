#include <QApplication>
#include <QtMultimedia/QMediaPlayer>
#include <QAudioOutput>
#include <MyButton.h>

void play(){
    auto* player = new QMediaPlayer();
    player->setSource(QUrl::fromLocalFile("click.wav"));
    auto* audio = new QAudioOutput();
    player->setAudioOutput(audio);
    audio->setVolume(100);
    player->play();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MyButton* button = new MyButton();

    button->resize(200,200);


    QObject::connect(button, &MyButton::down, [](){play();});
    QObject::connect(button, &MyButton::up, [](){play();});

    button->show();

    return a.exec();
}
