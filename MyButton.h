#include <QPushButton>
#include <QPaintEvent>
#include <QPainter>

class MyButton : public QPushButton{
    Q_OBJECT
public:
    MyButton(){
        connect(this,&QPushButton::pressed,[this](){
            buttonPixmap = downPixmap;
            update();
            emit down();
        });
        connect(this,&QPushButton::clicked,[this](){
            buttonPixmap = upPixmap;
            update();
            emit up();
        });
    }

signals:
    void up();
    void down();

private:
    QPixmap upPixmap = QPixmap("button_up.png");
    QPixmap downPixmap = QPixmap("button_down.png");
    QPixmap buttonPixmap = upPixmap;

    void paintEvent(QPaintEvent *e) override{
        QPainter p(this);
        p.drawPixmap(e->rect(),buttonPixmap);
    }

};
